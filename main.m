%% Parameters

% --- Changeable
prm_frangi_threshold = 0.25;

prm_max_gap_filling_length = 40;   % Length in pixels
prm_max_gap_filling_angle  = 120.0;
prm_max_gap_filling_cost   = 4000; % Image-based cost

prm_manual_membrane_margin   = 10;
prm_max_distance_to_membrane = 20;
prm_min_edge_length          = 15;

prm_plot_skel_as_graph = false;

% --- Change with caution
prm_delta_lin_tophat  = 10; % Delta angle in degrees
prm_length_lin_tophat = 10; % In pixels

prm_frangi_options = struct('FrangiScaleRange', [1 3], ...
                            'FrangiScaleRatio', 0.5,  ...
                            'FrangiBetaOne',    0.5,  ...
                            'FrangiBetaTwo',    15,   ...
                            'verbose',          false, ...
                            'BlackWhite',       false);

prm_max_overlap = 0.15; % [0, 0.5]

prm_use_segm_cache = false;
prm_use_cache      = false;

%% Cyto images
[img_path, img_name, img_stacks, cell_mip, x_res, y_res, done] = CytonemeImage();
if done
    return
end
mip_dim = size(cell_mip);

null_img = zeros(mip_dim, 'uint8');

%% Next steps
fprintf('Next step(s): Membrane Detection or Drawing\n')

%% Membrane images
membrane_img_name = MembraneImageName(img_path);
manual_membrane   = isempty(membrane_img_name);

if manual_membrane
    [cell_is_on_left, cell_mask, done] = MembraneDelineation(cell_mip);
    if done
        return
    end
    
    tic
else
    tic
    
    [membrane_mip, cell_is_on_left, initial_cell_mask, cell_mask] = MembraneSegmentation(membrane_img_name, cell_mip, prm_use_segm_cache);

    % COMMENT OUT
    %ShowPartialResult('Initial Active Contour', membrane_img, initial_cell_mask, null_img);
    ShowPartialResult('Membrane Detection',   cell_mip,      cell_mask,         null_img);
end

%% Make sure cell is on the left-hand side
if ~cell_is_on_left
    fprintf('/!\\ CELL IS ON RIGHT SIDE:\n/!\\ ALL IMAGES WILL BE FLIPPED TO PUT CELL ON LEFT SIDE\n')
    img_stacks = fliplr(img_stacks);
    cell_mip    = fliplr(cell_mip);
    
    if ~manual_membrane
        membrane_mip = fliplr(membrane_mip);
        initial_cell_mask = fliplr(initial_cell_mask);
    end

    cell_mask  = fliplr(cell_mask);
    
    cell_is_on_left = true;
end
cell_mask = int8(cell_mask > 0);
CELL_MASK = imdilate(cell_mask, strel('square', 3));
cyto_mask = 1 - cell_mask;
cell_distance_map = bwdist(cell_mask, 'euclidean');


%% Next steps
toc, fprintf('Next step(s): Image Processing\n')

%% Top hat (Transformation to enhance linear structures)
mip_enhanced = zeros(mip_dim);
for theta = 0:prm_delta_lin_tophat:(180 - prm_delta_lin_tophat)
    current_angle = imtophat(cell_mip, strel('line', prm_length_lin_tophat, theta));
    mip_enhanced  = max(mip_enhanced, current_angle);
end

%% Frangi filter
mip_frangi = FrangiFilter2D(mip_enhanced, prm_frangi_options);
mip_thresholded = double(mip_frangi >= prm_frangi_threshold);
mip_thresholded = mip_thresholded .* double(cyto_mask);

%% Cleaning binarized cyto mip
disk_se = strel('disk', 1);
mip_cleaned = imclose(mip_thresholded, disk_se); % Closes small holes
mip_cleaned = imopen(mip_cleaned,      disk_se); % Removes small and objects and smoothes the others

%% Next steps
toc, fprintf('Next step(s): Skeletonization\n')

%% Cyto skeletonization
if prm_use_cache && exist('skeletonization.mat', 'file')
    fprintf('/!\\ Loading skeletonization.mat instead of skeletonizing the mask image...\n')
    load('skeletonization.mat', 'cytoneme_pieces', 'skeleton_CCs', 'cc_endpoints')
else
%
skeleton_mask = int8(bwmorph(mip_cleaned, 'skel', Inf));
skeleton_mask = CleanSkeletonMask(skeleton_mask);

[cytoneme_pieces, skeleton_CCs] = InitialCytonemeStructs(skeleton_mask);
skeleton_lmsk = labelmatrix(skeleton_CCs);

[~, skeleton_nodes, skeleton_edges] = Skel2Graph3D(skeleton_mask, 0);
end_point_coords = EndPointCoords(skeleton_nodes);
[cytoneme_pieces, cc_endpoints] = CCsRootsAndLeaves(cytoneme_pieces, skeleton_lmsk, end_point_coords);

if prm_plot_skel_as_graph
    PlotSkeletonGraph(skeleton_nodes, skeleton_edges, mip_dim), drawnow
end

save('skeletonization.mat', 'cytoneme_pieces', 'skeleton_CCs', 'cc_endpoints')
end

% COMMENT OUT
%figure, imagesc(labelmatrix(skeleton_CCs)), colormap(colorcube), title('Initial Skeletons'), drawnow

%% Next steps
toc, fprintf('Next step(s): Reconnection to Membrane\n')

%% Reconnection to the membrane
cytoneme_pieces = CCsReconnectedToMembrane(cytoneme_pieces, cell_mip, cell_distance_map, prm_max_distance_to_membrane);

fprintf('%d CC(s) connected to the membrane on %d CCs\n', NRootCCs(cytoneme_pieces), length(cytoneme_pieces))
% COMMENT OUT
%ShowPartialResult('Reconnection to Membrane', img_mip, ...
%    MaskRebuiltFromCytoStructs(LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'AM')), cell_mask);

%% Next steps
toc, fprintf('Next step(s): Gap Filling\n')

%% Gap Filling with Dijkstra
[connection_mask, ~, ~] = NormalizedConnectionMask(MaskRebuiltFromCytoStructs(LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'AM')), 'init');
n_unrooted = 0;

while ~NoMoreJoinable(cytoneme_pieces)
    idx_o_closest = ClosestUnlinkedCCToMembrane(cytoneme_pieces, cc_endpoints, cell_distance_map);
    
    rightmost_gap_coords = cc_endpoints{idx_o_closest}(1,:);
    right_cc_col_extent  = max(cc_endpoints{idx_o_closest}(2:end,2)) - cc_endpoints{idx_o_closest}(1,2);

    [all_leftmost_gap_coords, all_leftmost_gap_ep_cc] = ...
        PotentialCCLeavesForConnection(cytoneme_pieces, idx_o_closest, cc_endpoints, ...
            rightmost_gap_coords, right_cc_col_extent, ...
            prm_max_gap_filling_length, prm_max_gap_filling_angle, prm_max_overlap);
    if isempty(all_leftmost_gap_coords)
        cytoneme_pieces(idx_o_closest).not_joinable = true;
        continue
    end
    
    assert(~any(all_leftmost_gap_ep_cc == idx_o_closest))
        
    [idx_of_min, min_gap_cost, gap_coords] = BestConnection(cell_mip, ...
        LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'AMJ'), ...
        CELL_MASK, all_leftmost_gap_coords, rightmost_gap_coords);

    if min_gap_cost <= prm_max_gap_filling_cost % Note that min_gap_cost can be Inf
        cc_idx = all_leftmost_gap_ep_cc(idx_of_min);
        lin_coords = sub2ind(mip_dim, gap_coords(:,1), gap_coords(:,2));
        
        if cytoneme_pieces(cc_idx).root_label > 0
            root_label = cytoneme_pieces(cc_idx).root_label;
        else
            root_label = length(cytoneme_pieces) + n_unrooted + 1;
            n_unrooted = n_unrooted + 1;
        end

        connection_mask(lin_coords) = min_gap_cost;

        assert(cytoneme_pieces(cc_idx).label == cc_idx)
        assert(cytoneme_pieces(idx_o_closest).label == idx_o_closest)
        
        cytoneme_pieces(cc_idx).successor_labels(end+1)    = idx_o_closest;

        cytoneme_pieces(idx_o_closest).root_label          = root_label;
        cytoneme_pieces(idx_o_closest).ancestor_label      = cc_idx;
        cytoneme_pieces(idx_o_closest).junction_lin_coords = lin_coords;
        cytoneme_pieces(idx_o_closest).junction_cost       = min_gap_cost;
    else
        cytoneme_pieces(idx_o_closest).not_joinable = true;
    end
end

cytoneme_pieces = FillAllSuccesorLabelsForRoots(cytoneme_pieces);

ShowPartialResult('Skeleton with Gapfilling on Image', ...
    cell_mip, MaskRebuiltFromCytoStructs(LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'AMJ')), cell_mask);

[connection_mask, min_cost, max_cost] = NormalizedConnectionMask(connection_mask, 'final');
fprintf('Min/max connection cost: %f/%f\n', min_cost, max_cost)
figure, imagesc(connection_mask + 0.5 * max(connection_mask(:)) * double(cell_mask)), title('Skeleton with Gapfilling (with costs)'), drawnow
% COMMENT OUT
%figure, imagesc(LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'RMJ')), title('Skeleton with Gapfilling (with labeling)'), drawnow

%% Next steps
toc, fprintf('Next step(s): Pruning\n')

%% Pruning
cytoneme_pieces = PrunedSkeletonMask(cytoneme_pieces, CELL_MASK, prm_min_edge_length);
skeleton_lmsk   = LMaskRebuiltFromCytoStructs(cytoneme_pieces, mip_dim, 'RMJP');

if manual_membrane
    skeleton_lmsk(cell_distance_map < prm_manual_membrane_margin) = 0;
    
    next_available_pos = length(cytoneme_pieces) + 1;
    all_labels = transpose(unique(skeleton_lmsk));
    all_labels(1) = [];
    for label = all_labels
        cc_mask = int8(skeleton_lmsk == label);
        connected_cmp = bwconncomp(cc_mask, 8);
        if connected_cmp.NumObjects > 1
            %cc_lmsk = labelmatrix(connected_cmp);
            for idx = 2:connected_cmp.NumObjects
                skeleton_lmsk(connected_cmp.PixelIdxList{idx}) = next_available_pos;
                % Note: the following structure is largely incorrectly
                % filled. For example, the successors should be looked for
                % among the successors of the first connected component.
                cytoneme_pieces(next_available_pos) = struct(...
                         'label',            next_available_pos, ...
                         'lin_coords',       connected_cmp.PixelIdxList{idx},   ...
                         'tree_root',        [], ...            % Piece considered as itself
                         'tree_leaves',      [], ...            % Piece considered as itself
                         'is_root',          true, ... % Piece considered as part of a cytoneme
                         'ancestor_label',   next_available_pos, ...
                         'successor_labels', [],            ... % Immediate successors
                         ... % Below: Only meaningful if is_root
                         'lin_coords_to_membrane', [], ...
                         'missing_to_membrane',    0, ...
                         'all_successor_labels',   [], ...
                         'pruned_entirely',        false, ...
                         'pruned_lin_coords',      [], ... % From itself or any of its successors
                         ... % Below: Only meaningful if not is_root
                         'root_label',          next_available_pos, ...
                         'not_joinable',        false,           ...
                         'junction_lin_coords', [],                      ... % Backward junction (i.e., toward ancestor)
                         'junction_cost',       0  ...
                        );
                next_available_pos = next_available_pos + 1;
            end
%             region_props = regionprops(connected_cmp, 'Area', 'PixelIdxList');
%             tree_lengths = [region_props.Area];
%             [~, idx_o_longest] = max(tree_lengths);
%             to_remove  = setdiff(1:connected_cmp.NumObjects, idx_o_longest);
%             for idx = to_remove
%                 skeleton_lmsk(region_props(idx).PixelIdxList) = 0;
%             end
        end
    end

    [cytoneme_pieces, skeleton_lmsk] = FakelyReconnectedLMask(cytoneme_pieces, skeleton_lmsk, cell_mip, cell_distance_map);
%     [skeleton_lmsk, renew_to_new_lbl] = RelabeledLMask(skeleton_lmsk);
%     for idx = 1:length(renew_to_new_lbl)
%         renew_to_new_lbl(idx) = new_to_old_lbl(renew_to_new_lbl(idx));
%     end
%     new_to_old_lbl = renew_to_new_lbl;
end

[skeleton_lmsk, new_to_old_lbl] = RelabeledLMask(skeleton_lmsk);

final_skeleton_for_display = ShowPartialResult('Final Cytoneme Detection', ...
    cell_mip, MaskRebuiltFromCytoStructs(skeleton_lmsk), cell_mask);
skeleton_w_label = skeleton_lmsk;
skeleton_w_label(skeleton_w_label == 0) = 255;
final_l_skeleton_for_display = ShowPartialResult('Final Labeled Cytoneme Detection', ...
    skeleton_w_label, double(skeleton_w_label) / 255.0, skeleton_w_label);

%% Next steps
toc, fprintf('Next step(s): Measurements and Output\n')

%% Measures
OutputResults(cytoneme_pieces, skeleton_lmsk, new_to_old_lbl, img_name, ...
    final_skeleton_for_display, final_l_skeleton_for_display, x_res, y_res)

toc
