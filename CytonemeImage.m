function [img_path, img_name, img_stacks, img_mip, x_res, y_res, done] = CytonemeImage()

done = false;
img_stacks = [];
img_mip = [];

[img_name, img_path, ~] = uigetfile({'*.*'}, 'Select Cytomenes Image');
if img_name == 0
    fprintf('NO CYTONEME IMAGE SELECTED. NOTHING TO BE DONE\n')
    done = true;
    return
end

full_img_name = fullfile(img_path, img_name);
img_info      = imfinfo(full_img_name);
n_stacks      = numel(img_info);
fprintf('Image %dx%d with %d stacks (if many stacks, loading will take some time)\n', ...
        img_info(1).Width, img_info(1).Height, n_stacks)
if isfield(img_info, 'XResolution') && isfield(img_info, 'YResolution') && isfield(img_info, 'ResolutionUnit')
    x_res = 1.0 / img_info(1).XResolution;
    y_res = 1.0 / img_info(1).YResolution;
    fprintf('X/Y resolutions = %f/%f (Unit=%s)\n', x_res, y_res, img_info(1).ResolutionUnit)
else
    x_res = 0.0;
    y_res = 0.0;
end

img_stacks = zeros(img_info(1).Height, img_info(1).Width, n_stacks);
for stack_idx = 1:n_stacks
    img_stacks(:,:,stack_idx) = imread(full_img_name, stack_idx, 'Info', img_info);
end

% Maximun Intensity Projection (MIP)
img_mip = max(img_stacks, [], 3);
