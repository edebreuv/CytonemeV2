function [new_cytoneme_pieces, linked_skeleton, lnk_skeleton_lmsk, lbl_correspondences] = ...
    RemoveUnlinkedCCs(cytoneme_pieces, cc_close_to_membrane, skeleton_lmsk)

new_cytoneme_pieces = cytoneme_pieces;
lnk_skeleton_lmsk   = skeleton_lmsk;

for idx = 1:length(new_cytoneme_pieces)
    if new_cytoneme_pieces(idx).root_label > length(new_cytoneme_pieces)
        %assert(any(lnk_skeleton_lmsk(:) ==
        %new_cytoneme_pieces(idx).root_label)) % /!\ put this back TODO
        %FIXME
        lnk_skeleton_lmsk(lnk_skeleton_lmsk == new_cytoneme_pieces(idx).root_label) = 0;
        new_cytoneme_pieces(idx).dangling = true;
    elseif new_cytoneme_pieces(idx).root_label == 0
        assert(any(lnk_skeleton_lmsk(:) == new_cytoneme_pieces(idx).label))
        lnk_skeleton_lmsk(lnk_skeleton_lmsk == new_cytoneme_pieces(idx).label) = 0;
        new_cytoneme_pieces(idx).dangling = true;
    end
end

% labels = unique(lnk_skeleton_lmsk);
% labels(1) = []; % Remove background label
% unlinked_labels = setdiff(labels, find(cc_close_to_membrane));
% for idx = 1:length(unlinked_labels)
%     lnk_skeleton_lmsk(lnk_skeleton_lmsk == unlinked_labels(idx)) = 0;
%     
%     assert(new_cytoneme_pieces(unlinked_labels(idx)).label == unlinked_labels(idx))
%     new_cytoneme_pieces(unlinked_labels(idx)).dangling = true;
% end

[lnk_skeleton_lmsk, lbl_correspondences] = RelabeledLMask(lnk_skeleton_lmsk); % Some labels might have disappeared

linked_skeleton = int8(lnk_skeleton_lmsk > 0);
