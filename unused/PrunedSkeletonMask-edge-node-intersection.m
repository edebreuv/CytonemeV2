function [pruned_skel_mask, pruned_skel_lmsk] = PrunedSkeletonMask(skeleton_mask, skeleton_lmsk, CELL_MASK, min_edge_length)

pruned_skel_mask = skeleton_mask;

[~, skeleton_nodes, skeleton_edges] = Skel2Graph3D(skeleton_mask, 0);
edge_lengths = cellfun(@length, {skeleton_edges.point});
short_edges  = edge_lengths < min_edge_length;


full_inter_ep = 0;
full_exter_ep = 0;
partial_inter_ep = 0;
full_inter = 0;
full_exter = 0;
partial_inter = 0;


for idx = 1:length(skeleton_edges)
    if short_edges(idx)
        current_edge = skeleton_edges(idx);
        node_1 = skeleton_nodes(current_edge.n1);
        node_2 = skeleton_nodes(current_edge.n2);
        
        a = intersect(current_edge.point, node_1.idx);
        if length(a) == length(node_1.idx)
            if node_1.ep == 1
                full_inter_ep = full_inter_ep + 1;
            else
                full_inter = full_inter + 1;
            end
        elseif ~isempty(a)
            if node_1.ep == 1
                partial_inter_ep = partial_inter_ep + 1;
            else
                partial_inter = partial_inter + 1;
            end
        else
            if node_1.ep == 1
                full_exter_ep = full_exter_ep + 1;
            else
                full_exter = full_exter + 1;
            end
        end
        b = intersect(current_edge.point, node_2.idx);
        if length(b) == length(node_2.idx)
            if node_2.ep == 1
                full_inter_ep = full_inter_ep + 1;
            else
                full_inter = full_inter + 1;
            end
        elseif ~isempty(b)
            if node_2.ep == 1
                partial_inter_ep = partial_inter_ep + 1;
            else
                partial_inter = partial_inter + 1;
            end
        else
            if node_2.ep == 1
                full_exter_ep = full_exter_ep + 1;
            else
                full_exter = full_exter + 1;
            end
        end
            
        
        n1_is_ep = (node_1.ep == 1);
        n2_is_ep = (node_2.ep == 1);
        
        if n1_is_ep && n2_is_ep
            pruned_skel_mask(current_edge.point) = 0;
            pruned_skel_mask(node_1.idx) = 0;
            pruned_skel_mask(node_2.idx) = 0;
        elseif n1_is_ep
            if all(CELL_MASK(node_1.idx) == 0)
                pruned_skel_mask(current_edge.point) = 0;
                pruned_skel_mask(node_1.idx) = 0;
            end
        elseif n2_is_ep
            if all(CELL_MASK(node_2.idx) == 0)
                pruned_skel_mask(current_edge.point) = 0;
                pruned_skel_mask(node_2.idx) = 0;
            end
        end
    end
end


fprintf('\nPRUNED SKELETON: EP %d %d %d / %d %d %d on %d\n\n', full_inter_ep, partial_inter_ep, full_exter_ep, full_inter, partial_inter, full_exter, 2*sum(short_edges))




pruned_skel_mask = CleanSkeletonMask(pruned_skel_mask);

pruned_skel_lmsk = skeleton_lmsk .* uint16(pruned_skel_mask);
pruned_skel_lmsk = RelabeledLMask(pruned_skel_lmsk);
