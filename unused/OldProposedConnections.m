function OldProposedConnections()

% gray_levels_on_skeleton = img_mip(skeleton_mask > 0);
% min_gray_on_skel = min(gray_levels_on_skeleton);
% max_gray_on_skel = max(gray_levels_on_skeleton);
% 
% connexion_list = cell(0, 4);
% connexion_next_available = 1;
% n_discarded_for_distance = 0;
% n_pairs = 0.5 * n_connected_comp * (n_connected_comp - 1);
% connection_mask = - double(rebuilt_cyto_skeleton);
% 
% for cc_one_idx = 1:(n_connected_comp - 1)
%     toc, fprintf('    CC.%d/%d - Discarded = %d/%d\n', cc_one_idx, n_connected_comp - 1, n_discarded_for_distance, n_pairs)
%     for cc_two_idx = (cc_one_idx + 1):n_connected_comp
%         if cc_endpoints{cc_one_idx}(1,1) < cc_endpoints{cc_two_idx}(1,1)
%             leftmost_gap_coords  = cc_endpoints{cc_one_idx}(2:end,:);
%             rightmost_gap_coords = cc_endpoints{cc_two_idx}(1,:);
%             cc_from_left_right   = [cc_one_idx, cc_two_idx];
%         else
%             leftmost_gap_coords  = cc_endpoints{cc_two_idx}(2:end,:);
%             rightmost_gap_coords = cc_endpoints{cc_one_idx}(1,:);
%             cc_from_left_right   = [cc_two_idx, cc_one_idx];
%         end
%         
%         segments = bsxfun(@minus, leftmost_gap_coords, rightmost_gap_coords);
%         norms = vecnorm(segments, 2, 2);
%         too_large_segment_idc = norms > prm_max_gap_filling_length;
%         
%         leftmost_gap_coords(too_large_segment_idc,:) = [];
%         if isempty(leftmost_gap_coords)
%             n_discarded_for_distance = n_discarded_for_distance + 1;
%             continue
%         end
% 
%         angle_validity = true(1, size(leftmost_gap_coords, 1));
%         for left_ep_idx = 1:size(leftmost_gap_coords, 1)
%             [angle_in_degrees, ~] = getAngle(leftmost_gap_coords(left_ep_idx,:), rightmost_gap_coords);
%             if abs(angle_in_degrees) > prm_max_gap_filling_angle
%                 angle_validity(left_ep_idx) = false;
%             end
%         end
% 
%         leftmost_gap_coords(~angle_validity,:) = [];
%         if isempty(leftmost_gap_coords)
%             n_discarded_for_distance = n_discarded_for_distance + 1;
%             continue
%         end
%         
%         all_leftmost_gap_coords    = leftmost_gap_coords;
%         all_search_window_vertices = cell(1, size(all_leftmost_gap_coords, 1));
%         all_gap_costs              =  Inf(1, size(all_leftmost_gap_coords, 1));% Inf: in case step (*) is taken on every loop
%         all_shortest_paths         = cell(1, size(all_leftmost_gap_coords, 1));
%         
%         %toc, fprintf('        Before %d shortest paths\n', size(all_leftmost_gap_coords, 1))
%         for left_ep_idx = 1:size(all_leftmost_gap_coords, 1)
%             leftmost_gap_coords = all_leftmost_gap_coords(left_ep_idx,:);
% 
%             if rightmost_gap_coords(2) >= leftmost_gap_coords(2)
%                 row_step = 1;
%             else
%                 row_step = -1;
%             end
%             [all_rows, all_cols] = ndgrid(leftmost_gap_coords(2):row_step:rightmost_gap_coords(2), ...
%                                           leftmost_gap_coords(1):rightmost_gap_coords(1));
%             search_window_vertices = [all_cols(:), all_rows(:)];
%             assert(rebuilt_cyto_skeleton(search_window_vertices(1,2),   search_window_vertices(1,1))   == 1)
%             assert(rebuilt_cyto_skeleton(search_window_vertices(end,2), search_window_vertices(end,1)) == 1)
% 
%             for vertex_idx = 2:(size(search_window_vertices, 1) - 1)
%                 col = search_window_vertices(vertex_idx, 1);
%                 row = search_window_vertices(vertex_idx, 2);
%                 if rebuilt_cyto_skeleton(row, col) > 0
%                     continue% (*)
%                 end
%             end
% 
%             n_edges = 0;
%             edge_weights = zeros(size(search_window_vertices, 1) * (size(search_window_vertices, 1) - 1), 3);
%             for p = 1:(size(search_window_vertices, 1) - 1)
%                 for q = (p+1):size(search_window_vertices, 1)
%                     edge_length = norm(search_window_vertices(p,:) - search_window_vertices(q,:));
%                     if edge_length < 1.5
%                         n_edges = n_edges + 1;
%                         edge_weights(n_edges, 1) = p;
%                         edge_weights(n_edges, 2) = q;
% 
%                         gray_level = img_mip(search_window_vertices(q, 2), search_window_vertices(q, 1));
%                         if gray_level < min_gray_on_skel
%                             edge_weights(n_edges, 3) = edge_length + (min_gray_on_skel / max(1, gray_level));
%                         elseif gray_level < max_gray_on_skel
%                             edge_weights(n_edges, 3) = edge_length + (max_gray_on_skel - gray_level) / (max_gray_on_skel - min_gray_on_skel);
%                         else
%                             edge_weights(n_edges, 3) = edge_length;
%                         end
%                     end
%                 end
%             end
% 
%             all_search_window_vertices{left_ep_idx} = search_window_vertices;
%             [all_gap_costs(left_ep_idx), all_shortest_paths{left_ep_idx}] = dijkstra(search_window_vertices, edge_weights(1:n_edges, :), 1, size(search_window_vertices, 1));
% %             if gap_cost > prm_max_gap_filling_cost
% %     %             for idx = 2:(length(shortest_path) - 1)
% %     %                 coords = search_window_vertices(shortest_path(idx),:);
% %     %                 rejected_connections(coords(2), coords(1)) = 2;
% %     %             end
% %     %             fprintf('--- Gap filling rejected with cost %f\n', gap_cost)
% %                 continue
% %             end
%         end
%         %toc, fprintf('        After %d shortest paths\n', size(all_leftmost_gap_coords, 1))
%         
%         [min_gap_cost, idx_of_min] = min(all_gap_costs);
%         if min_gap_cost > prm_max_gap_filling_cost% Note that min_gap_cost can be Inf (see above)
%             continue
%         end
%         
%         shortest_path = all_shortest_paths{idx_of_min};
%         search_window_vertices = all_search_window_vertices{idx_of_min};
%         
%         connexion_list{connexion_next_available, 1} = cc_from_left_right(1);
%         connexion_list{connexion_next_available, 2} = cc_from_left_right(2);
%         connexion_list{connexion_next_available, 3} = min_gap_cost;
%         connexion_list{connexion_next_available, 4} = search_window_vertices(shortest_path(2:(end - 1)),:);
% %         connexion_list{connexion_next_available, 4} = zeros(length(shortest_path) - 2, 2);
% %         for idx = 2:(length(shortest_path) - 1)
% %             coords = search_window_vertices(shortest_path(idx),:);
% %             connexion_list{connexion_next_available, 4}(idx-1,:) = coords(:);
% %             %rebuilt_cyto_skeleton(coords(2), coords(1)) = 1;
% %         end
%         connexion_next_available = connexion_next_available + 1;
%     end
% end
% 
% gap_costs = [connexion_list{:,3}];
% [~, sorting_idc] = sort(gap_costs);
% connexion_list = connexion_list(sorting_idc, :);
% assert(connexion_list{1,3} <= connexion_list{end,3})
% 
% while ~isempty(connexion_list)
%     gap_cost = connexion_list{1, 3};
%     
%     lin_coords = sub2ind(mip_dim, connexion_list{1, 4}(:,2), connexion_list{1, 4}(:,1));
%     rebuilt_cyto_skeleton(lin_coords) = 1;
%     connection_mask(lin_coords) = gap_cost;        
% %     for idx = 1:size(connexion_list{1, 4}, 1)
% %         coords = connexion_list{1, 4}(idx,:);
% %         rebuilt_cyto_skeleton(coords(2), coords(1)) = 1;
% %         connection_mask(coords(2), coords(1)) = gap_cost;        
% %     end
%     right_ccs = [connexion_list{:,2}];
%     connexion_list(right_ccs == connexion_list{1, 2}, :) = [];
% end
% 
% max_cost = max(connection_mask(:));
% connection_mask(connection_mask == -1) = Inf;
% min_cost = min(connection_mask(:));
% fprintf('Min/max connection cost: %f/%f\n', min_cost, max_cost)
% 
% connection_mask(isinf(connection_mask)) = max_cost + 0.2 * (max_cost - min_cost);
% 
% filled_cyto_skeleton = rebuilt_cyto_skeleton;
