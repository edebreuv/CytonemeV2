function skeleton_mask = RebuiltSkeletonMaskFromGraph(graph)

skeleton_mask = graph;

% end_points_idc = find([skeleton_nodes.ep] == 1);
% [skeleton_edges(:).assigned] = deal(0);
% rebuilt_cyto_skeleton = zeros(cyto_mip_dim);
% branch_idx = 1;
% 
% for ep_idx = 1:length(end_points_idc)
%     node_idx = end_points_idc(ep_idx);
% 
%     while true            
%         current_node = skeleton_nodes(node_idx);
%         idc_of_edge_to_visit = current_node.edges_idc;
%         
%         if isempty(idc_of_edge_to_visit)
%             break
%         elseif length(idc_of_edge_to_visit) > 1
%             leftmost_ep_cols = [];
%             for edge_idx = 1:length(idc_of_edge_to_visit)
%                 [~, ~, ~, ~, ~, ~, local_lm_col] = LeftmostEndPoint(skeleton_edges, idc_of_edge_to_visit(edge_idx), cyto_skeleton, cell_is_on_left);
%                 leftmost_ep_cols = [leftmost_ep_cols; local_lm_col];
%             end
%             [~, idc_of_leftmost] = min(leftmost_ep_cols);
%             idc_of_edge_to_visit = idc_of_edge_to_visit(idc_of_leftmost(1));
%         end
%         
%         current_edge = skeleton_edges(idc_of_edge_to_visit);
%         if current_edge.assigned > 0
%             break
%         end
% 
%         if current_edge.n1 == node_idx
%             if skeleton_nodes(current_edge.n2).comy > current_node.comy % wrong orientation
%                 break
%             end
%         else
%             if skeleton_nodes(current_edge.n1).comy > current_node.comy % wrong orientation
%                 break
%             end
%         end
% 
%         rebuilt_cyto_skeleton(current_edge.point) = branch_idx;
%         rebuilt_cyto_skeleton(current_node.idx)   = branch_idx;
%         skeleton_edges(idc_of_edge_to_visit).assigned = branch_idx;
%         branch_idx = branch_idx + 1;
% 
%         if node_idx == current_edge.n1
%             node_idx = current_edge.n2;
%         else
%             node_idx = current_edge.n1;
%         end
%     end
% end
