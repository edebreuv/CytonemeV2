function [idx_of_min, min_gap_cost, gap_coords] = BestForwardConnection(img_mip, skeleton_mask, dilated_skeleton_mask, all_gap_left_eps, gap_right_ep)

all_sw_vertices    = cell(1, size(all_gap_left_eps, 1));
all_gap_costs      =  Inf(1, size(all_gap_left_eps, 1));% Inf: in case step (*) is taken on every loop
all_shortest_paths = cell(1, size(all_gap_left_eps, 1));

max_gray = max(img_mip(:));

for left_ep_idx = 1:size(all_gap_left_eps, 1)
    gap_left_ep = all_gap_left_eps(left_ep_idx,:);

    if gap_right_ep(1) >= gap_left_ep(1)
        row_overall_dir = 1;
        min_row = gap_left_ep(1);
        max_row = gap_right_ep(1);
    else
        row_overall_dir = -1;
        min_row = gap_right_ep(1);
        max_row = gap_left_ep(1);
    end
    [all_rows, all_cols] = ndgrid(gap_left_ep(1):row_overall_dir:gap_right_ep(1), ...
                                  gap_left_ep(2):gap_right_ep(2));
    sw_vertices = [all_rows(:), all_cols(:)]; % sw=search window
    
    assert(skeleton_mask(sw_vertices(1,1),   sw_vertices(1,2))   == 1)
    assert(skeleton_mask(sw_vertices(end,1), sw_vertices(end,2)) == 1)

    min_sw_row = min(all_rows(:));
    max_sw_row = max(all_rows(:));
    min_sw_col = min(all_cols(:));
    sw_size = [max_sw_row - min_sw_row + 1, max(all_cols(:)) - min_sw_col + 1];
    if row_overall_dir > 0
        row_conversion_cst = min_sw_row - 1;
    else
        row_conversion_cst = max_sw_row + 1;
    end

    assert(sub2ind(sw_size, row_overall_dir * (gap_left_ep(1)  - row_conversion_cst), gap_left_ep(2)  - min_sw_col + 1) == 1)
    assert(sub2ind(sw_size, row_overall_dir * (gap_right_ep(1) - row_conversion_cst), gap_right_ep(2) - min_sw_col + 1) == size(sw_vertices, 1))
    
    n_edges = 0;
    edge_weights = zeros(3 * numel(sw_vertices), 3);
    for vertex_idx = 1:(size(sw_vertices, 1) - 1)
        if sw_vertices(vertex_idx,2) == gap_right_ep(2)
            continue
        end
        if (vertex_idx > 1) && (dilated_skeleton_mask(sw_vertices(vertex_idx,1), sw_vertices(vertex_idx,2)) > 0)
            continue
        end

        for row_incr = -1:1
            new_row = sw_vertices(vertex_idx,1) + row_incr;
            new_col = sw_vertices(vertex_idx,2) + 1;
            if (new_row < min_row) || (new_row > max_row)
                continue
            end
            new_v_idx = sub2ind(sw_size, row_overall_dir * (new_row - row_conversion_cst), new_col - min_sw_col + 1);
            if (new_v_idx < size(sw_vertices, 1)) && (dilated_skeleton_mask(new_row, new_col) > 0)
                continue
            end
            
            n_edges = n_edges + 1;
            edge_weights(n_edges, 1) = vertex_idx;
            edge_weights(n_edges, 2) = new_v_idx;
            edge_weights(n_edges, 3) = max_gray - img_mip(new_row, new_col) + 1;
            
            assert(sum((sw_vertices(vertex_idx,:) - sw_vertices(edge_weights(n_edges, 2),:)).^2, 2) <= 2)
        end
    end
    if n_edges == 0
        continue
    end

    all_sw_vertices{left_ep_idx} = sw_vertices;
    [all_gap_costs(left_ep_idx), all_shortest_paths{left_ep_idx}] = dijkstra(sw_vertices, edge_weights(1:n_edges, :), 1, size(sw_vertices, 1));

    shortest_path = all_shortest_paths{left_ep_idx};
    gap_coords    = sw_vertices(shortest_path(2:(end - 1)),:);
    lin_coords    = sub2ind(size(skeleton_mask), gap_coords(:,1), gap_coords(:,2));
    assert(~any(sum(diff(gap_coords, 1, 1).^2, 2) > 2))
    assert(~any(skeleton_mask(lin_coords) > 0))
%     if any(sum(diff(gap_coords, 1, 1).^2, 2) > 2)
%         keyboard
%     end
%     if any(skeleton_mask(lin_coords) > 0)
%         keyboard
%     end
end

[min_gap_cost, idx_of_min] = min(all_gap_costs);

sw_vertices   = all_sw_vertices{idx_of_min};
shortest_path = all_shortest_paths{idx_of_min};
gap_coords    = sw_vertices(shortest_path(2:(end - 1)),:);
