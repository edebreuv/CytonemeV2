%% Parameters

prm_frangi_threshold = 0.25;

prm_max_gap_filling_length = 60;
prm_max_gap_filling_angle  = 60.0;
prm_max_gap_filling_cost   = 35.0;

prm_max_distance_to_membrane = 20;

%% Cyto images
[img_path, img_stacks, img_mip] = CytonemeImage();
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
img_mip = img_mip(450:500,:);
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
% /////////////!!!!!!!!!!!!!!\\\\\\\\\\\\\\\
mip_dim = size(img_mip);

null_img = zeros(mip_dim, 'uint8');

%% Next steps
fprintf('Next step(s): Membrane Detection or Drawing\n')

%% Membrane images
membrane_img_name = MembraneImageName(img_path);

if membrane_img_name ~= 0
    tic
    
    [membrane_img, cell_is_on_left, initial_cell_mask, cell_mask] = MembraneSegmentation(membrane_img_name, mip_dim);

    ShowPartialResult('Initial Active Contour', membrane_img, initial_cell_mask, null_img)
    ShowPartialResult('Final Active Contour',   img_mip,      cell_mask,         null_img)
else
    [cell_is_on_left, cell_mask] = MembraneDelineation(img_mip);
    
    tic
end

%% Make sure cell is on the left-hand side
if ~cell_is_on_left
    fprintf('/!\\ CELL IS ON RIGHT SIDE:\n/!\\ ALL IMAGES WILL BE FLIPPED TO CELL ON LEFT SIDE\n')
    img_stacks = fliplr(img_stacks);
    img_mip    = fliplr(img_mip);
    
    if membrane_img_name ~= 0
        membrane_img = fliplr(membrane_img);
        initial_cell_mask = fliplr(initial_cell_mask);
    end

    cell_mask  = fliplr(cell_mask);
    
    cell_is_on_left = true;
end
cell_mask = double(cell_mask > 0);
CELL_MASK = imdilate(cell_mask, strel('square', 3));
cyto_mask = 1.0 - cell_mask;

%% Next steps
toc, fprintf('Next step(s): Image Processing\n')

%% Top hat (Transformation to enhance linear structures)
mip_enhanced = zeros(mip_dim);
for theta = 0:10:170
    current_angle = imtophat(img_mip, strel('line', 10, theta));
    mip_enhanced  = max(mip_enhanced, current_angle);
end

%% Frangi filter
Options = struct('FrangiScaleRange', [1 3], ...
                 'FrangiScaleRatio', 0.5,  ...
                 'FrangiBetaOne',    0.5,  ...
                 'FrangiBetaTwo',    15,   ...
                 'verbose',          false, ...
                 'BlackWhite',       false);
mip_frangi = FrangiFilter2D(mip_enhanced, Options);
mip_thresholded = double(mip_frangi >= prm_frangi_threshold);
mip_thresholded = mip_thresholded .* cyto_mask;

% figure, imagesc(mip_thresholded), title('Thresholded Frangi'), drawnow

%% Cleaning binarized cyto mip
disk_se = strel('disk', 1);
mip_cleaned = imclose(mip_thresholded, disk_se); % Closes small holes
mip_cleaned = imopen(mip_cleaned,      disk_se); % Removes small and objects and smoothes the others

%% Next steps
toc, fprintf('Next step(s): Skeletonization\n')

%% Cyto skeletonization
skeleton_mask = bwmorph(mip_cleaned, 'skel', Inf);
skeleton_mask = CleanSkeletonMask(skeleton_mask);

figure, imagesc(skeleton_mask), title('Initial Skeletons'), drawnow

%% Next steps
toc, fprintf('Next step(s): Reconnection to Membrane\n')

%% Reconnection to the membrane
[skeleton_lmsk, cc_close_to_membrane] = CCsReconnectedToMembrane(img_mip, skeleton_mask, cell_mask, prm_max_distance_to_membrane);

ShowPartialResult('Reconnection to Membrane', img_mip, filled_cyto_skeleton, cell_mask)

%% Next steps
toc, fprintf('Next step(s): Gap Filling\n')

%% Gap Filling : Dijkstra
n_connected_comp = max(skeleton_lmsk(:));

end_points_idc = find([skeleton_nodes.ep] == 1);
end_point_coords = zeros(1, length(end_points_idc));
for idx = 1:length(end_points_idc)
    end_point_coords(idx) = skeleton_nodes(end_points_idc(idx)).idx(1);
    % /!\ For some reason, Skel2Graph3D can associate several pixels to a
    % leaf node, even if the skeleton contains only pixels that would break
    % connectivity if removed.
end
labeled_skel_at_eps = skeleton_lmsk(end_point_coords);

% end_point_coords = zeros(length(end_points_idc), 1);
% for ep_idx = 1:length(end_points_idc)
%     lin_idx = skeleton_nodes(end_points_idc(ep_idx)).idx; % linear idx
%     %[row, col] = ind2sub(mip_dim, lin_idx);
%     end_point_coords(ep_idx,:) = lin_idx; % [row, col];
% end

cc_endpoints = cell(1, n_connected_comp); % For each CC, first ep is root, subsequent are leaves
for cc_idx = 1:(n_connected_comp)
    cc_ep_idc = (labeled_skel_at_eps == cc_idx);
    if sum(cc_ep_idc) < 2
        [cc_rows, cc_cols] = find(skeleton_lmsk == cc_idx);
        sorted_cc_coords = sortrows([cc_cols cc_rows]);
        cc_endpoints{cc_idx} = [sorted_cc_coords(1, :); sorted_cc_coords(end, :)];
    else
        [rows, cols] = ind2sub(mip_dim, transpose(end_point_coords(cc_ep_idc)));
        cc_endpoints{cc_idx} = sortrows([cols rows]);
    end
end

% leftmost_ep_of_trees  = []; % Actually not necessarily end points since edges can have a hook shape
% rightmost_ep_of_trees = [];
% for label = 1:n_connected_comp
%     [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
%     sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
%     leftmost_ep_of_trees  = [leftmost_ep_of_trees;  sorted_skeleton_cols(1, :)];
%     rightmost_ep_of_trees = [rightmost_ep_of_trees; sorted_skeleton_cols(end, :)];
% end

gray_levels_on_skeleton = img_mip(rebuilt_cyto_skeleton > 0);
min_gray_on_skel = min(gray_levels_on_skeleton);
max_gray_on_skel = max(gray_levels_on_skeleton);

%rejected_connections = rebuilt_cyto_skeleton;

connexion_list = cell(0, 4);
connexion_next_available = 1;
n_discarded_for_distance = 0;
n_pairs = 0.5 * n_connected_comp * (n_connected_comp - 1);
connection_mask = - double(rebuilt_cyto_skeleton);

for cc_one_idx = 1:(n_connected_comp - 1)
    toc, fprintf('    CC.%d/%d - Discarded = %d/%d\n', cc_one_idx, n_connected_comp - 1, n_discarded_for_distance, n_pairs)
    for cc_two_idx = (cc_one_idx + 1):n_connected_comp
        if cc_endpoints{cc_one_idx}(1,1) < cc_endpoints{cc_two_idx}(1,1)
            leftmost_gap_coords  = cc_endpoints{cc_one_idx}(2:end,:);
            rightmost_gap_coords = cc_endpoints{cc_two_idx}(1,:);
            cc_from_left_right   = [cc_one_idx, cc_two_idx];
        else
            leftmost_gap_coords  = cc_endpoints{cc_two_idx}(2:end,:);
            rightmost_gap_coords = cc_endpoints{cc_one_idx}(1,:);
            cc_from_left_right   = [cc_two_idx, cc_one_idx];
        end
        
        segments = bsxfun(@minus, leftmost_gap_coords, rightmost_gap_coords);
        norms = vecnorm(segments, 2, 2);
        too_large_segment_idc = norms > prm_max_gap_filling_length;
        
        leftmost_gap_coords(too_large_segment_idc,:) = [];
        if isempty(leftmost_gap_coords)
            n_discarded_for_distance = n_discarded_for_distance + 1;
            continue
        end

        angle_validity = true(1, size(leftmost_gap_coords, 1));
        for left_ep_idx = 1:size(leftmost_gap_coords, 1)
            [angle_in_degrees, ~] = getAngle(leftmost_gap_coords(left_ep_idx,:), rightmost_gap_coords);
            if abs(angle_in_degrees) > prm_max_gap_filling_angle
                angle_validity(left_ep_idx) = false;
            end
        end

        leftmost_gap_coords(~angle_validity,:) = [];
        if isempty(leftmost_gap_coords)
            n_discarded_for_distance = n_discarded_for_distance + 1;
            continue
        end
        
        all_leftmost_gap_coords    = leftmost_gap_coords;
        all_search_window_vertices = cell(1, size(all_leftmost_gap_coords, 1));
        all_gap_costs              =  Inf(1, size(all_leftmost_gap_coords, 1));% Inf: in case step (*) is taken on every loop
        all_shortest_paths         = cell(1, size(all_leftmost_gap_coords, 1));
        
        %toc, fprintf('        Before %d shortest paths\n', size(all_leftmost_gap_coords, 1))
        for left_ep_idx = 1:size(all_leftmost_gap_coords, 1)
            leftmost_gap_coords = all_leftmost_gap_coords(left_ep_idx,:);

            if rightmost_gap_coords(2) >= leftmost_gap_coords(2)
                row_step = 1;
            else
                row_step = -1;
            end
            [all_rows, all_cols] = ndgrid(leftmost_gap_coords(2):row_step:rightmost_gap_coords(2), ...
                                          leftmost_gap_coords(1):rightmost_gap_coords(1));
            search_window_vertices = [all_cols(:), all_rows(:)];
            assert(rebuilt_cyto_skeleton(search_window_vertices(1,2),   search_window_vertices(1,1))   == 1)
            assert(rebuilt_cyto_skeleton(search_window_vertices(end,2), search_window_vertices(end,1)) == 1)

            for vertex_idx = 2:(size(search_window_vertices, 1) - 1)
                col = search_window_vertices(vertex_idx, 1);
                row = search_window_vertices(vertex_idx, 2);
                if rebuilt_cyto_skeleton(row, col) > 0
                    continue% (*)
                end
            end

            n_edges = 0;
            edge_weights = zeros(size(search_window_vertices, 1) * (size(search_window_vertices, 1) - 1), 3);
            for p = 1:(size(search_window_vertices, 1) - 1)
                for q = (p+1):size(search_window_vertices, 1)
                    edge_length = norm(search_window_vertices(p,:) - search_window_vertices(q,:));
                    if edge_length < 1.5
                        n_edges = n_edges + 1;
                        edge_weights(n_edges, 1) = p;
                        edge_weights(n_edges, 2) = q;

                        gray_level = img_mip(search_window_vertices(q, 2), search_window_vertices(q, 1));
                        if gray_level < min_gray_on_skel
                            edge_weights(n_edges, 3) = edge_length + (min_gray_on_skel / max(1, gray_level));
                        elseif gray_level < max_gray_on_skel
                            edge_weights(n_edges, 3) = edge_length + (max_gray_on_skel - gray_level) / (max_gray_on_skel - min_gray_on_skel);
                        else
                            edge_weights(n_edges, 3) = edge_length;
                        end
                    end
                end
            end

            all_search_window_vertices{left_ep_idx} = search_window_vertices;
            [all_gap_costs(left_ep_idx), all_shortest_paths{left_ep_idx}] = dijkstra(search_window_vertices, edge_weights(1:n_edges, :), 1, size(search_window_vertices, 1));
%             if gap_cost > prm_max_gap_filling_cost
%     %             for idx = 2:(length(shortest_path) - 1)
%     %                 coords = search_window_vertices(shortest_path(idx),:);
%     %                 rejected_connections(coords(2), coords(1)) = 2;
%     %             end
%     %             fprintf('--- Gap filling rejected with cost %f\n', gap_cost)
%                 continue
%             end
        end
        %toc, fprintf('        After %d shortest paths\n', size(all_leftmost_gap_coords, 1))
        
        [min_gap_cost, idx_of_min] = min(all_gap_costs);
        if min_gap_cost > prm_max_gap_filling_cost% Note that min_gap_cost can be Inf (see above)
            continue
        end
        
        shortest_path = all_shortest_paths{idx_of_min};
        search_window_vertices = all_search_window_vertices{idx_of_min};
        
        connexion_list{connexion_next_available, 1} = cc_from_left_right(1);
        connexion_list{connexion_next_available, 2} = cc_from_left_right(2);
        connexion_list{connexion_next_available, 3} = min_gap_cost;
        connexion_list{connexion_next_available, 4} = search_window_vertices(shortest_path(2:(end - 1)),:);
%         connexion_list{connexion_next_available, 4} = zeros(length(shortest_path) - 2, 2);
%         for idx = 2:(length(shortest_path) - 1)
%             coords = search_window_vertices(shortest_path(idx),:);
%             connexion_list{connexion_next_available, 4}(idx-1,:) = coords(:);
%             %rebuilt_cyto_skeleton(coords(2), coords(1)) = 1;
%         end
        connexion_next_available = connexion_next_available + 1;
    end
end

gap_costs = [connexion_list{:,3}];
[~, sorting_idc] = sort(gap_costs);
connexion_list = connexion_list(sorting_idc, :);
assert(connexion_list{1,3} <= connexion_list{end,3})

while ~isempty(connexion_list)
    gap_cost = connexion_list{1, 3};
    
    lin_coords = sub2ind(mip_dim, connexion_list{1, 4}(:,2), connexion_list{1, 4}(:,1));
    rebuilt_cyto_skeleton(lin_coords) = 1;
    connection_mask(lin_coords) = gap_cost;        
%     for idx = 1:size(connexion_list{1, 4}, 1)
%         coords = connexion_list{1, 4}(idx,:);
%         rebuilt_cyto_skeleton(coords(2), coords(1)) = 1;
%         connection_mask(coords(2), coords(1)) = gap_cost;        
%     end
    right_ccs = [connexion_list{:,2}];
    connexion_list(right_ccs == connexion_list{1, 2}, :) = [];
end

max_cost = max(connection_mask(:));
connection_mask(connection_mask == -1) = Inf;
min_cost = min(connection_mask(:));
fprintf('Min/max connection cost: %f/%f\n', min_cost, max_cost)

connection_mask(isinf(connection_mask)) = max_cost + 0.2 * (max_cost - min_cost);
%max_cost = max_cost + 0.2 * (max_cost - min_cost);
%connection_mask = uint8((100 * (connection_mask - min_cost) / (max_cost - min_cost)) + 100);

filled_cyto_skeleton = rebuilt_cyto_skeleton;

% tmp_for_display(:, :, 1) = uint8(cyto_mip);
% tmp_for_display(:, :, 2) = 128 * uint8(rejected_connections);
% tmp_for_display(:, :, 3) = 255 * uint8(cell_mask);
% figure, imagesc(rejected_connections), title('Rejected Connections (For Debugging Purposes)')

ShowPartialResult('Skeleton with Gapfilling on Image', img_mip, filled_cyto_skeleton, cell_mask)

% tmp_for_display(:, :, 1) = uint8(cyto_mip);
% tmp_for_display(:, :, 2) = connection_mask; %255 * uint8(filled_cyto_skeleton);
% tmp_for_display(:, :, 3) = 255 * uint8(cell_mask);
% figure, imagesc(tmp_for_display), title('Skeleton with Gapfilling on Image (with costs)'), drawnow
figure, imagesc(connection_mask), title('Skeleton with Gapfilling on Image (with costs)'), drawnow


%% Removing unconnected branches
skeleton_connected_cmp = double(labelmatrix(bwconncomp(filled_cyto_skeleton)));

ep_on_membrane_side = zeros(max(skeleton_connected_cmp(:)), 3);
for label = 1:max(skeleton_connected_cmp(:))
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
    ep_on_membrane_side(label,:) = [sorted_skeleton_cols(1,:), label];
end

for ep_idx = 1:size(ep_on_membrane_side, 1)
    end_point = ep_on_membrane_side(ep_idx,:);
    if CELL_MASK(end_point(2), end_point(1)) == 0
        filled_cyto_skeleton(skeleton_connected_cmp == end_point(3)) = 0;
    end
end

final_skeleton_for_display(:, :, 1) = uint8(img_mip);
final_skeleton_for_display(:, :, 2) = 255 * uint8(filled_cyto_skeleton);
final_skeleton_for_display(:, :, 3) = 255 * uint8(cell_mask);
figure, imagesc(final_skeleton_for_display), title('Detected Cytonemes (after removal of unconnected trees)'), drawnow

%% Next steps
toc, fprintf('Next step(s): Measurements and Output\n')

%% Labelling
skeleton_connected_cmp = double(labelmatrix(bwconncomp(filled_cyto_skeleton)));

%% Measures
measures = regionprops(skeleton_connected_cmp, 'Area');
tree_lengths = [measures.Area];
median_tree_length = median(tree_lengths);

measures = zeros(max(skeleton_connected_cmp(:)), 2);
for label = 1:max(skeleton_connected_cmp(:))
    measures(label,:) = [label, tree_lengths(label)];
end
table_of_lengths = array2table(measures, 'VariableNames', {'Label', 'Length'}); % Length of cytonemes (Pixels)

horiz_dist_to_membrane = zeros(max(skeleton_connected_cmp(:)), 2);
for label = 1:max(skeleton_connected_cmp(:))
    [skeleton_rows, skeleton_cols] = find(skeleton_connected_cmp == label);
    sorted_skeleton_cols = sortrows([skeleton_cols skeleton_rows]);
    distance = sorted_skeleton_cols(end, 1) - sorted_skeleton_cols(1, 1);% First and last points of each label
    horiz_dist_to_membrane(label, :) = [label, distance];
end
median_h_dist = median(horiz_dist_to_membrane(:, 2));
table_of_h_dist = array2table(horiz_dist_to_membrane, 'VariableNames', {'Label', 'Distance'});

n_branches_per_tree = zeros(max(skeleton_connected_cmp(:)), 2);
for label = 1:max(skeleton_connected_cmp(:))
    skeleton_tree = skeleton_connected_cmp == label;
    [~, ~, junction_coords] = anaskel(skeleton_tree);
    junction_lin_idc = sub2ind(mip_dim, junction_coords(2, :), junction_coords(1, :));
    skeleton_tree(junction_lin_idc) = 0;
    tree_connected_cmp = labelmatrix(bwconncomp(double(skeleton_tree)));
    n_branches_per_tree(label,:) = [label, max(tree_connected_cmp(:))];
end
mean_n_branches = mean(n_branches_per_tree(:, 2));
table_of_n_branches = array2table(n_branches_per_tree, 'VariableNames', {'Label', 'Number_of_branches'});

%% Registration of figures
% From: https://www.mathworks.com/matlabcentral/fileexchange/15885-get-user-home-directory
% By: Sven Probst
if ispc
    userDir = winqueryreg('HKEY_CURRENT_USER', ...
                         ['Software\Microsoft\Windows\CurrentVersion\' ...
                          'Explorer\Shell Folders'], 'Personal');
else
    userDir = char(java.lang.System.getProperty('user.home'));
end
result_folder  = fullfile(userDir, 'Desktop', ['Results', '-', strrep(strrep(datestr(datetime), ' ', '-'), ':', '-')]);
%result_folder = ['C:\Users\eric\Desktop\Results', '-', strrep(strrep(datestr(datetime), ' ', '-'), ':', '-')];
mkdir(result_folder)
current_folder = cd(result_folder);

imwrite(final_skeleton_for_display, 'cytonemes.png')
writetable(table_of_h_dist, 'distances_to_membrane.txt')
writetable(table_of_lengths, 'cytonemes_lengths.txt')
writetable(table_of_n_branches, 'cytonemes_branches.txt')

cd(current_folder)

%% Display Results
disp({'---------------'
      'Cytoneme detection'
      '--------------'
     ['Total number = ',                     num2str(max(skeleton_connected_cmp(:)))];
     ['Average number of branches = ',       num2str(mean_n_branches)];
     ['Median length of cytonemes = ',       num2str(median_tree_length)];
     ['Median distance to the membrane =  ', num2str(median_h_dist)];
      '---------------'})

toc
