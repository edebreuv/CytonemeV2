function [cell_is_on_left, cell_mask, done] = MembraneDelineation(img_mip)

done = false;

while (true)
    [cell_mask, cell_is_on_left] = Mask_Cell(img_mip);
    if isempty(cell_mask)
        fprintf('NO CELL MASK. NOTHING TO BE DONE\n')
        done = true;
        return
    end

    figure, imagesc(double(128*cell_mask)+img_mip), colormap(gray)

    opts.Interpreter = 'tex';
    opts.Default = 'Yes';
    answer = questdlg('Are you satisfied with your membrane tracing ?', ...
                      'Confirmation', 'Yes', 'No', opts);
    if strcmp(answer, 'Yes')
        close(gcf)
        break
    end
    close(gcf)
end
